import React, {Component} from 'react';
import './loginPage.css';
import axios from 'axios';

export class LandingPage extends Component {

    constructor(props){
        super(props);
    
        this.state = {
            username: '',
            password: '',
            errorState: false,
        }
      }

    handleChange = (e) => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    onLoginClick = (e) => {
        e.preventDefault();
        axios({
            method: 'post',
            url: 'http://localhost:8080/api/auth',
            data: {
                korisnickoIme: this.state.username,
                lozinka: this.state.password,
            }
          }).then((response) => {
              localStorage.setItem('bank_jwt', response.data.token)
              window.location.replace('/dashboard')
            })
            .catch((error) => {
              console.log(error)    
              alert('Wrong username or password')
            })
        
    }

    render() {
        return (
            <div className='frame'>
                <div className='wrapper-login'>
                <p className="title">FTNBank</p> 
                    <form>
                    <div className="inputWithIcon">
                        <input
                            className="singnin-input"
                            name='username' 
                            placeholder="Username"
                            value={this.state.username}
                            onChange={this.handleChange.bind(this)}/>  
                            <i class="fa fa-user fa-lg fa-fw"  aria-hidden="true"></i>
                    </div>
                    <div className="inputWithIcon">
                        <input
                            className="singnin-input"
                            name='password' 
                            placeholder="Password"
                            type="password"
                            value={this.state.password}
                            onChange={this.handleChange.bind(this)}/> 
                            <i class="fas fa-lock" aria-hidden="true"></i> 
                    </div>       
                    <p className={this.state.errorState ? "" : "error-message"} >Wrong username or password </p>
                        <button class="sign-in-btn" onClick={this.onLoginClick}>Sign In</button>
                    </form>
                </div>
            </div>
          );
    }

}


export default LandingPage;
