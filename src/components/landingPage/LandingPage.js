import React from 'react';
import Nav from './LandingPageNav';
import './LandingPage.css';
import HomePage from './landingHome/HomePage'


function LandingPage() {
  return (
    <div className = "wrapper">
        <Nav></Nav>
        <HomePage></HomePage>
    </div>
  );
}


export default LandingPage;
