import React, { Component } from 'react';
import './LandingPageNav.css';
import './LinkHover.css';


export class LandingPageNav extends Component {

    login(){
        window.location.replace('/login');
    }
  
    render() {
        return (
            <div >
                <div className="logo-wrapper">
                    <h1>FTNBank</h1>
                </div>
                <div className="nav-wrapper">
                <div onClick={this.login} className="login-btn">
                    <p>Log In</p>
                </div>  
                    <ul className="nav-links">
                        <li><a href="#" className="hover-effect">Home</a> </li>
                        <li><a href="#" className="hover-effect">Services</a> </li>
                        <li><a href="#" className="hover-effect">Citizens</a> </li>
                        <li><a href="#" className="hover-effect">Business</a> </li>                
                    </ul>
                    
                </div>
                
            </div>
          );
    }
    
}

export default LandingPageNav;