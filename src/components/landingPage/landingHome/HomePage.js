import React from 'react';
import './HomePage.css'


function HomePage() {
  return (
    <div className="home-page-wrapper">
        <h1 className="home-quote">Bank That Cares About You</h1>        
    </div>
  );
}


export default HomePage;
