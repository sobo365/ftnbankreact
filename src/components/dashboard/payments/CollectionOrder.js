import React, { Component } from 'react';
import '../Dashboard.css';
import './Payments.css';
import Select from "@material-ui/core/Select";
import axios from "axios";
import MenuItem from "@material-ui/core/MenuItem";
import MessageDialog from '../dialogs/MessageDialog';

export class CollectionOrder extends Component {

  constructor(props) {
    super(props);

    this.state = {
      jedan: new String(),
      dva: new String(),
      tri: new String(),
      cetiri: new String,
      pet: new String,
      sest: new String,
      sedam: new String,
      osam: new String,
      devet: new String,
      deset: new String,
      jedanaes: new String,
      sifraPlacanja: new String(),
      valuta: "valuta",
      iznos: new String(),
      racunPrimaoca: new String(),
      model: new String(),
      pozivNaBroj: new String(),
      errors: true,
      currency: [],
      selectedCurrency: 2,
      mesta: [],
      selectedMesto: 1,
    };
  }

  discard() {
    window.location.replace('/dashboard')
  }

  componentDidMount = () => {
    axios({
      method: "get",
      url: "http://localhost:8080/api/valute",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("bank_jwt"),
      },
    })
      .then((response) => {
        console.log(response);
        this.setState({
          currency: response.data,
        });
      })
      .catch(function (error) {
        console.log(error);
        this.refs.MessageDialog.handleToggle('Greška prilikom učitavanja valuta!', true);
      })
      .then(function () {
        // always executed
      });

    axios({
      method: "get",
      url: "http://localhost:8080/api/mesta",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("bank_jwt"),
      },
    })
      .then((response) => {
        console.log(response);
        this.setState({
          mesta: response.data,
        });
      })
      .catch(function (error) {
        console.log(error);
        this.refs.MessageDialog.handleToggle('Greška prilikom učitavanja mesta!', true);
      })
      .then(function () {
        // always executed
      });
  };

  getCurrnecyObject = (id) => {
    for (let i = 0; i < this.state.currency.length; i++) {
      if (this.state.currency[i].id === id) {
        return this.state.currency[i];
      }
    }
  };

  getMestoObject = (id) => {
    for (let i = 0; i < this.state.mesta.length; i++) {
      if (this.state.mesta[i].id === id) {
        return this.state.mesta[i];
      }
    }
  };

  renderCurrency = () => {
    let components = [];
    for (let i = 0; i < this.state.currency.length; i++) {
      let curr = this.state.currency[i];
      let val = {
        id: curr.id,
        nazivValute: curr.nazivValute,
        skraceniNaziv: curr.skraceniNaziv,
      };
      components.push(
        <MenuItem key={i} value={curr.id}>
          {curr.skraceniNaziv} - {curr.nazivValute}
        </MenuItem>
        //value={curr.id}>{curr.skraceniNaziv} - {curr.nazivValute}
      );
    }
    return components;
  };

  renderPlaces = () => {
    let components = [];
    for (let i = 0; i < this.state.mesta.length; i++) {
      let mesto = this.state.mesta[i];
      components.push(
        <MenuItem key={i} value={mesto.id}>
          {mesto.nazivMesta}
        </MenuItem>
        //value={curr.id}>{curr.skraceniNaziv} - {curr.nazivValute}
      );
    }
    return components;
  };


  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  submit = () => {
    axios({
      method: "POST",
      url: "http://localhost:8080/api/nalog-prenos",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("bank_jwt"),
      },
      data: {
        nalogodavacDuznik: this.state.jedan,
        svrhaPlacanja: this.state.dva,
        poverilacPrimalac: this.state.tri,
        sifraPlacanja: this.state.cetiri,
        valuta: this.getCurrnecyObject(this.state.selectedCurrency),
        iznos: this.state.pet,
        racunDuznika: this.state.sest,
        modelZaduzenja: this.state.sedam,
        pozivNaBrojZaduzenja: this.state.osam,
        racunPoverioca: this.state.devet,
        modelOdobrenja: this.state.deset,
        pozivNaBrojOdobrenja: this.state.jedanaest,
        mesto: this.getMestoObject(this.state.selectedMesto),
      },
    })
      .then((response) => {
        this.refs.MessageDialog.handleToggle('Transakcija uspešno izvršena!', false);
      })
      .catch( (error) => {
        console.log(error.response);
        try{
          this.refs.MessageDialog.handleToggle(error.response.data.greske.greska, true);
        } catch {
          this.refs.MessageDialog.handleToggle('Greška na serveru!', true);
        }
      })
      .then(function () {
        // always executed
      });
  };


  render() {
    return (
      <div className="dashboard-container-empty">
      <MessageDialog ref = 'MessageDialog'></MessageDialog> 
      <div className = "header">
        <p className = 'client-header-title'>NALOG ZA NAPLATU</p>
        <button  onClick={this.submit} className = 'submit-btn'>Submit</button>
          <button onClick={this.discard} className = 'discard-btn'>Discard</button>
       </div>
       <div className="leftSide">
        <p>Duznik</p>
        <textarea
          name="jedan"
          onChange={this.handleChange.bind(this)}
          value={this.state.jedan}
          spellCheck={false}
          maxLength="200"
          className={
            this.state.jedan ? "input-field" : "input-field-error"
          }
        />
        <p>Svrha Uplate</p>
        <textarea
          name="dva"
          onChange={this.handleChange.bind(this)}
          value={this.state.dva}
          spellCheck={false}
          maxLength="200"
          className={
            this.state.dva ? "input-field" : "input-field-error"
          }
        />
        <p>Poverilac - Nalogodavac</p>
        <textarea
          name="tri"
          onChange={this.handleChange.bind(this)}
          value={this.state.primalac}
          spellCheck={false}
          maxLength="200"
          className={
            this.state.tri ? "input-field" : "input-field-error"
          }
        />
      </div>
      {/* _____________RIGHT */}
      <div className="rightSide">
        <div className="row-wrapper">
          <div
            style={{
              width: "15%",
              marginRight: "4%",
              display: "inline-block",
            }}
          >
            <p>Sifra placanja</p>
            <input
              value={this.state.cetiri}
              className="input-field"
              name="cetiri"
              value={this.state.cetiri}
              onChange={this.handleChange.bind(this)}
            ></input>
          </div>
          <div
            style={{
              width: "15%",
              marginRight: "4%",
              display: "inline-block",
            }}
          >
            <p>Valuta</p>
            <Select
              value={this.selectedCurrency}
              defaultValue={2}
              labelId="demo-simple-select-label"
              onChange={this.handleChange.bind(this)}
              name="selectedCurrency"
            >
              {this.renderCurrency()}
            </Select>
          </div>
          <div style={{ width: "60%", display: "inline-block" }}>
            <p>Iznos</p>
            <input
              value={this.state.pet}
              className="input-field"
              name="pet"
              onChange={this.handleChange.bind(this)}
            ></input>
          </div>
        </div>
        <div className="row-wrapper">
          <p>Racun duznika</p>
          <input
            value={this.state.sest}
            className="input-field"
            name="sest"
            onChange={this.handleChange.bind(this)}
          ></input>
        </div>

        <div className="row-wrapper">
          <div
            style={{
              width: "15%",
              marginRight: "4%",
              display: "inline-block",
            }}
          >
            <p>Model</p>
            <input
              value={this.state.sedam}
              className="input-field"
              name="sedam"
              onChange={this.handleChange.bind(this)}
            ></input>
          </div>
          <div
            style={{
              width: "75%",
              marginRight: "4%",
              display: "inline-block",
            }}
          >
            <p>Poziv na broj - zaduženje</p>
            <input
              value={this.state.osam}
              className="input-field"
              name="osam"
              onChange={this.handleChange.bind(this)}
            ></input>
          </div>
        </div>

        <div className="row-wrapper">
          <p>Racun poverioca - nalogodavca</p>
          <input
            value={this.state.devet}
            className="input-field"
            name="devet"
            onChange={this.handleChange.bind(this)}
          ></input>
        </div>

        <div className="row-wrapper">
          <div
            style={{
              width: "15%",
              marginRight: "4%",
              display: "inline-block",
            }}
          >
            <p>Model</p>
            <input
              value={this.state.deset}
              className="input-field"
              name="deset"
              onChange={this.handleChange.bind(this)}
            ></input>
          </div>
          <div
            style={{
              width: "75%",
              marginRight: "4%",
              display: "inline-block",
            }}
          >
            <p>Poziv na broj - odobrenje</p>
            <input
              value={this.state.jedanaest}
              className="input-field"
              name="jedanaest"
              onChange={this.handleChange.bind(this)}
            ></input>
          </div>
        </div>

        <div
          style={{ width: "15%", marginRight: "4%", display: "inline-block" }}
        >
          <p>Mesto</p>
          <Select
            value={this.selectedMesto}
            defaultValue={2}
            labelId="demo-simple-select-label"
            onChange={this.handleChange.bind(this)}
            name="selectedMesto"
          >
            {this.renderPlaces()}
          </Select>
        </div>
      </div>
    </div>
    )
}

  
}





export default CollectionOrder;