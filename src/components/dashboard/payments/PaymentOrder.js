import React, { Component } from "react";
import "../Dashboard.css";
import "./Payments.css";
import { validate, Validator } from "../../../util/Validator";
import Select from "@material-ui/core/Select";
import axios from "axios";
import MenuItem from "@material-ui/core/MenuItem";
import MessageDialog from '../dialogs/MessageDialog';

export class PaymentOrder extends Component {
  constructor(props) {
    super(props);

    this.state = {
      uplatilac: new String(),
      svrhaUplate: new String(),
      primalac: new String(),
      sifraPlacanja: new String(),
      valuta: "valuta",
      iznos: new String(),
      racunPrimaoca: new String(),
      model: new String(),
      pozivNaBroj: new String(),
      errors: true,
      currency: [],
      selectedCurrency: 2,
      mesta: [],
      selectedMesto: 1,
    };
  }

  renderCurrency = () => {
    let components = [];
    for (let i = 0; i < this.state.currency.length; i++) {
      let curr = this.state.currency[i];
      let val = {
        id: curr.id,
        nazivValute: curr.nazivValute,
        skraceniNaziv: curr.skraceniNaziv,
      };
      components.push(
        <MenuItem key={i} value={curr.id}>
          {curr.skraceniNaziv} - {curr.nazivValute}
        </MenuItem>
        //value={curr.id}>{curr.skraceniNaziv} - {curr.nazivValute}
      );
    }
    return components;
  };

  renderPlaces = () => {
    let components = [];
    for (let i = 0; i < this.state.mesta.length; i++) {
      let mesto = this.state.mesta[i];
      components.push(
        <MenuItem key={i} value={mesto.id}>
          {mesto.nazivMesta}
        </MenuItem>
        //value={curr.id}>{curr.skraceniNaziv} - {curr.nazivValute}
      );
    }
    return components;
  };

  componentDidMount = () => {
    axios({
      method: "get",
      url: "http://localhost:8080/api/valute",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("bank_jwt"),
      },
    })
      .then((response) => {
        console.log(response);
        this.setState({
          currency: response.data,
        });
      })
      .catch(function (error) {
        console.log(error);
        this.refs.MessageDialog.handleToggle('Greška prilikom učitavanja valuta!', true);
      })
      .then(function () {
        // always executed
      });

    axios({
      method: "get",
      url: "http://localhost:8080/api/mesta",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("bank_jwt"),
      },
    })
      .then((response) => {
        console.log(response);
        this.setState({
          mesta: response.data,
        });
      })
      .catch(function (error) {
        console.log(error);
        this.refs.MessageDialog.handleToggle('Greška prilikom učitavanja mesta!', true);
      })
      .then(function () {
        // always executed
      });
  };

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  discard() {
    window.location.replace("/dashboard");
  }

  submit = () => {
    let nalog = {
      nalogodavacDuznik: this.state.uplatilac,
      svrhaPlacanja: this.state.svrhaUplate,
      poverilacPrimalac: this.state.primalac,
      sifraPlacanja: this.state.sifraPlacanja,
      valuta: this.getCurrnecyObject(this.state.selectedCurrency),
      iznos: this.state.iznos,
      racunPoverioca: this.state.racunPrimaoca,
      modelOdobrenja: this.state.model,
      pozivNaBrojOdobrenja: this.state.pozivNaBroj,
      mesto: this.getMestoObject(this.state.selectedMesto),
    };
    axios({
      method: "POST",
      url: "http://localhost:8080/api/nalog-uplata",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("bank_jwt"),
      },
      data: {
        nalogodavacDuznik: this.state.uplatilac,
        svrhaPlacanja: this.state.svrhaUplate,
        poverilacPrimalac: this.state.primalac,
        sifraPlacanja: this.state.sifraPlacanja,
        valuta: this.getCurrnecyObject(this.state.selectedCurrency),
        iznos: this.state.iznos,
        racunPoverioca: this.state.racunPrimaoca,
        modelOdobrenja: this.state.model,
        pozivNaBrojOdobrenja: this.state.pozivNaBroj,
        mesto: this.getMestoObject(this.state.selectedMesto),
      },
    })
      .then((response) => {
        this.refs.MessageDialog.handleToggle('Transakcija uspešno izvršena!', false);
      })
      .catch( (error) => {
        console.log(error.response);
        try{
          this.refs.MessageDialog.handleToggle(error.response.data.greske.greska, true);
        } catch {
          this.refs.MessageDialog.handleToggle('Greška na serveru!', true);
        }
      })
      .then(function () {
        // always executed
      });
  };

  getCurrnecyObject = (id) => {
    for (let i = 0; i < this.state.currency.length; i++) {
      if (this.state.currency[i].id === id) {
        return this.state.currency[i];
      }
    }
  };

  getMestoObject = (id) => {
    for (let i = 0; i < this.state.mesta.length; i++) {
      if (this.state.mesta[i].id === id) {
        return this.state.mesta[i];
      }
    }
  };

  render() {
    return (
      <div className="dashboard-container-empty">
        <MessageDialog ref = 'MessageDialog'></MessageDialog> 
        <div className="header">
          <p className="client-header-title">NALOG ZA UPLATU</p>
          <button onClick={this.submit} className="submit-btn">
            Submit
          </button>
          <button onClick={this.discard} className="discard-btn">
            Discard
          </button>
        </div>

        <div className="leftSide">
          <p>Uplatilac</p>
          <textarea
            name="uplatilac"
            onChange={this.handleChange.bind(this)}
            value={this.state.uplatilac.value}
            spellCheck={false}
            maxLength="200"
            className={
              this.state.uplatilac ? "input-field" : "input-field-error"
            }
          />
          <p>Svrha Uplate</p>
          <textarea
            name="svrhaUplate"
            onChange={this.handleChange.bind(this)}
            value={this.state.svrhaUplate}
            spellCheck={false}
            maxLength="200"
            className={
              this.state.svrhaUplate ? "input-field" : "input-field-error"
            }
          />
          <p>Primalac</p>
          <textarea
            name="primalac"
            onChange={this.handleChange.bind(this)}
            value={this.state.primalac}
            spellCheck={false}
            maxLength="200"
            className={
              this.state.primalac ? "input-field" : "input-field-error"
            }
          />
        </div>
        {/* ---------RIGHT */}
        <div className="rightSide">
          <div className="row-wrapper">
            <div
              style={{
                width: "15%",
                marginRight: "4%",
                display: "inline-block",
              }}
            >
              <p>Sifra placanja</p>
              <input
                value={this.state.sifraPlacanja}
                className="input-field"
                name="sifraPlacanja"
                value={this.state.sifraPlacanja}
                onChange={this.handleChange.bind(this)}
              ></input>
            </div>
            <div
              style={{
                width: "15%",
                marginRight: "4%",
                display: "inline-block",
              }}
            >
              <p>Valuta</p>
              <Select
                value={this.selectedCurrency}
                defaultValue={2}
                labelId="demo-simple-select-label"
                onChange={this.handleChange.bind(this)}
                name="selectedCurrency"
              >
                {this.renderCurrency()}
              </Select>
            </div>
            <div style={{ width: "60%", display: "inline-block" }}>
              <p>Iznos</p>
              <input
                value={this.state.iznos}
                className="input-field"
                name="iznos"
                onChange={this.handleChange.bind(this)}
              ></input>
            </div>
          </div>
          <div className="row-wrapper">
            <p>Racun primaoca</p>
            <input
              value={this.state.racunPrimaoca}
              className="input-field"
              name="racunPrimaoca"
              onChange={this.handleChange.bind(this)}
            ></input>
          </div>

          <div className="row-wrapper">
            <div
              style={{
                width: "15%",
                marginRight: "4%",
                display: "inline-block",
              }}
            >
              <p>Model</p>
              <input
                value={this.state.model}
                className="input-field"
                name="model"
                onChange={this.handleChange.bind(this)}
              ></input>
            </div>
            <div
              style={{
                width: "75%",
                marginRight: "4%",
                display: "inline-block",
              }}
            >
              <p>Poziv na broj</p>
              <input
                value={this.state.pozivNaBroj}
                className="input-field"
                name="pozivNaBroj"
                onChange={this.handleChange.bind(this)}
              ></input>
            </div>
          </div>

          <div
            style={{ width: "15%", marginRight: "4%", display: "inline-block" }}
          >
            <p>Mesto</p>
            <Select
              value={this.selectedMesto}
              defaultValue={2}
              labelId="demo-simple-select-label"
              onChange={this.handleChange.bind(this)}
              name="selectedMesto"
            >
              {this.renderPlaces()}
            </Select>
          </div>
        </div>
      </div>
    );
  }
}

export default PaymentOrder;
