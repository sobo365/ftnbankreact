import React, { Component, Fragment } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import axios from 'axios';
import IconButton from '@material-ui/core/IconButton';
import CreditCardIcon from '@material-ui/icons/CreditCard';
import Divider from '@material-ui/core/Divider';
import AccountCard from './AccountCard'
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import AddBoxRoundedIcon from '@material-ui/icons/AddBoxRounded';
import AccountForm from '../accounts/AccountForm'
import './Accounts.css';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import ErrorIcon from '@material-ui/icons/Error';

export class MessageDialog extends Component {

    state = {
        open: this.props.open,
        content: '',
        error: true,
    }

    handleToggle = (content, error) => {
        this.setState({
            open : true,
            content: content,
            error : error,
        })
       this.forceUpdate();
    }

    closeDialog = () => {
        this.setState({
            open : false
        })
    }

    render() {
         const{open} = this.state
        return (
            <Fragment>
                <Dialog
                 PaperProps = {{
                    style: {
                        borderRadius: '20px',
                        width: '400px',
                        height: '200px',
                        background: '#fff'
                      },
                }}
                    fullScreen 
                    open={open}
                    onClose={() => {this.closeDialog()}}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    
                    <DialogContent >
                    <CheckBoxIcon 
                        style={{
                            width: '100px',
                            height: '100px',
                            marginLeft: '120px',
                            display: this.state.error ? 'none' : 'inline-block' , 
                            color:'#76FF03'}}></CheckBoxIcon>
                    <ErrorIcon
                        style={{
                            width: '100px',
                            height: '100px',
                            marginLeft: '120px',
                            display: this.state.error ? 'inline-block' : 'none', 
                            color:'#f44336'}}></ErrorIcon>
                            <Divider></Divider>
                        <p style={{textAlign:'center'}}>{this.state.content}</p>                    
                    </DialogContent>
                </Dialog>               
                
            </Fragment>
        )
    }

    accStyle = {
        background: '#000'
    }


   
}

export default MessageDialog