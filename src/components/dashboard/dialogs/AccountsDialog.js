import React, { Component, Fragment } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import axios from 'axios';
import IconButton from '@material-ui/core/IconButton';
import CreditCardIcon from '@material-ui/icons/CreditCard';
import Divider from '@material-ui/core/Divider';
import AccountCard from './AccountCard'
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import AddBoxRoundedIcon from '@material-ui/icons/AddBoxRounded';
import AccountForm from '../accounts/AccountForm'
import './Accounts.css';



const backgrounds = [
    'linear-gradient(45deg, rgba(229,57,53,1) 28%, rgba(233,30,99,1) 75%)',
    'linear-gradient(45deg, rgba(26,35,126,1) 28%, rgba(2,136,209,1) 75%)',
    'linear-gradient(45deg, rgba(255,234,0 ,1) 28%, rgba(255,145,0 ,1) 75%',
    'linear-gradient(45deg, rgba(118,255,3 ,1) 28%, rgba(198,255,0 ,1) 75%)',
    'linear-gradient(45deg, rgba(57,73,171,1) 28%, rgba(33,150,243,1) 75%)',
    'linear-gradient(45deg, rgba(251,140,0,1) 28%, rgba(255,87,34,1) 75%)',
    'linear-gradient(45deg, rgba(94,53,177,1) 28%, rgba(63,81,181,1) 75%)', 
    'linear-gradient(45deg, rgba(56,142,60,1) 28%, rgba(0,137,123,1) 75%)'    
]

const shadows = [
    'rgba(229,57,53,1)',
    'rgba(26,35,126,1)',
    'rgba(255,234,0 ,1)',
    'rgba(118,255,3 ,1)',
    'rgba(57,73,171,1)',
    'rgba(251,140,0,1)',
    'rgba(94,53,177,1)',
    'rgba(56,142,60,1)'
]

export class AccountsDialog extends Component {

    state = {
        open: false,
        acccount_id: 0,
        card_state: false,
        bg: '#fff',
        clr: '#fff',
     //   cards_hidden: false,
        state_cards: true,
        state_account: false,
        state_add_account: false
    }


    handleToggle = () => {
        if(this.state.state_cards) {
            this.setState({
                open: !this.state.open
            }) ;
        } else if (this.state.state_account) {
            this.setState({
                bg: '#fff',
                state_cards: true,
                state_account: false,
                state_add_account: false
            })
            
        } else if (this.state.state_add_account) {
            this.setState({
                bg: '#fff',
                state_cards: true,
                state_account: false,
                state_add_account: false
            })
        }
        
    }

    // handleToggleCard = () => {
    //     this.setState({
    //       //  cardState: !this.state.card_state
    //       state_cards: true,
    //       state_account: false,
    //       state_add_account: false
    //     })
    // }

    // handleOpenCard = (position) => {
    //     this.setState({
    //         bg: backgrounds[position],
    //         clr: shadows[position],
    //         //cards_hidden: true
    //         state_cards: false,
    //     state_account: true,
    //     state_add_account: false
    //     })
    // }

    handleAddAccount = () => {
        this.setState({
            state_cards: false,
            state_account: false,
            state_add_account: true
        })
    }
   

    renderAccounts = () => {
        let components = [];
        for(let i = 0; i < this.props.user.racuni.length ; i++){
      //    components.push(<AccountCard key={i} user={this.props.user} position={i} openCard={this.handleOpenCard.bind(this)} background={backgrounds[i]} shadow={shadows[i]}></AccountCard>)
          components.push(<AccountCard key={i} user={this.props.user} position={i} racuni={this.props.racuni} racun={this.props.user.racuni[i]}  background={backgrounds[i]} shadow={shadows[i]}></AccountCard>)
        }
    
        return components;
    }

    render() {
        const{open} = this.state
        return (
            <Fragment>
              
                <IconButton onClick={this.handleToggle}>
                    <CreditCardIcon fontSize="large" style={{color:'#000'}}/>
                  </IconButton>
                <Dialog
                 PaperProps = {{
                    style: {
                        borderRadius: '20px',
                        width: '94vw',
                        height: '90vh',
                      },
                }}
                    fullScreen 
                    open={open}
                    onClose={this.handleToggle}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{"Accounts"}
                    <IconButton onClick={this.handleAddAccount} >
                     <AddBoxRoundedIcon fontSize="large" style={{color:'#616161 '}}/>
                    </IconButton>
                    <Button style={{float:'right'}} color="secondary" onClick={this.handleToggle}>
                        Close
                    </Button>
                    </DialogTitle>
                    <Divider></Divider>
                    <DialogContent >
                    <div style={{width: '100%', display: this.state.state_cards ? 'inline-block' :'none' }}>
                    {this.renderAccounts()}
                    </div>

                    {/* <div style={{display: this.state.state_account ? 'inline-block' :'none', width: '100%', height: '100%' }}> */}
                        {/* <div className='account-data' style={{background: this.state.bg}}> */}
                            {/* <p className='data-text'>Owner: {this.props.user.ime}</p>   */}
                            {/* <p className='data-text'>Number: {this.props.user.racuni[this.state.position]}</p>   */}
                            {/* <p className='data-text'>Currency:  </p>   */}
                            {/* <p className='data-text'>Balance: </p>   */}
                            {/* <Button  variant="contained" size="medium" style={{color:this.state.clr, background: '#fff', bottom: '0', width: '70%', marginLeft: '12%', marginBottom: '5%', position: 'absolute'}} > */}
                                {/* Disable Account */}
                            {/* </Button> */}
                        {/* </div> */}
                        {/* <div className='account-transactions'> */}
                            {/* <p style={{margin: '0'}}>Daily Balance</p> */}
                        {/* </div> */}
                    {/* </div> */}
                    <div className='add-account' style={{display: this.state.state_add_account ? 'inline-block' :'none' }}>
                        <AccountForm user = {this.props.user}></AccountForm>
                    </div>
                    
                    </DialogContent>
                </Dialog>               
                
            </Fragment>
        )
    }

    accStyle = {
        background: backgrounds[0]
    }


   
}

export default AccountsDialog