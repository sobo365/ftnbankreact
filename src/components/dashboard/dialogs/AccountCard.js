import React, { Component, Fragment } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TablePagination from "@material-ui/core/TablePagination";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import axios from "axios";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import AspectRatioIcon from "@material-ui/icons/AspectRatio";
import "./Accounts.css";
import Select from "@material-ui/core/Select";
import MessageDialog from "../dialogs/MessageDialog";

export class AccountsDialog extends Component {
  state = {
    expanded: true,
    dnevnaStanja: [],
    disableAccount: true,
    racunZaPrenos: "",
    messageOpen: false,
  };

  getDnevnaStanja = () => {
    axios({
      method: "get",
      url:
        "http://localhost:8080/api/racuni/" +
        this.props.racun.id +
        "/dnevna-stanja",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("bank_jwt"),
      },
    })
      .then((response) => {
        console.log(response);
        if (response != null || response != undefined) {
          this.setState({
            dnevnaStanja: response.data,
          });
        }
      })
      .catch((error) => {
        console.log(error);
        try {
          this.openMessage(error.response.data.greske.greska, true);
        } catch {
          this.openMessage(
            "Greska na serveru prilikom učitavanja dnevnih stanja",
            true
          );
        }
      })
      .then(function () {
        // always executed
      });
  };

  componentDidMount = () => {
    this.getDnevnaStanja();
  };

  expand = () => {
    this.getDnevnaStanja();
    this.state.expanded = !this.state.expanded;
    this.forceUpdate();
    this.renderDnevnaStanja();
  };

  disableAcc = () => {
    this.state.disableAccount = !this.state.disableAccount;
    this.forceUpdate();
  };

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleGasenjeRacuna = () => {
    let params = {
      racunZaPrenos: this.state.racunZaPrenos,
      ugaseniRacun: this.props.racun.brojRacuna,
      podnosilacZahteva: "Bonko Zvogdan",
    };
    axios({
      method: "POST",
      url: "http://localhost:8080/api/ugaseni-racuni",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("bank_jwt"),
      },
      data: {
        prenosNaRacun: this.state.racunZaPrenos,
        ugaseniRacun: this.props.racun.brojRacuna,
        podnosilacZahteva: "Bonko Zvogdan",
      },
    })
      .then((response) => {
        this.openMessage("Racun je uspesno ugasen!", false);
      })
      .catch((error) => {
        try {
          this.openMessage(error.response.data.greske.greska, true);
        } catch {
          this.openMessage("Nedefinisana greska!", true);
        }
      });
  };

  openMessage = (message, error) => {
    this.refs.MessageDialog.handleToggle(message, error);
  };

  renderDnevnaStanja = () => {
    let components = [];
    for (let i = 0; i < this.state.dnevnaStanja.length; i++) {
      console.log(this.state.dnevnaStanja[i]);
      let item = this.state.dnevnaStanja[i];
      components.push(
        <TableRow key={i}>
          <TableCell align="right">{item.date}</TableCell>
          <TableCell align="right">
            {Math.ceil(item.novoStanje * 100) / 100}
          </TableCell>
          <TableCell align="right">
            {Math.ceil(item.prethodnoStanje * 100) / 100}
          </TableCell>
          <TableCell align="right">
            {Math.ceil(item.stanjeNaTeret * 100) / 100}
          </TableCell>
          <TableCell align="right">
            {Math.ceil(item.stanjeUKorist * 100) / 100}
          </TableCell>
        </TableRow>
      );
    }
    return components;
  };

  render() {
    return (
      <div>
        <MessageDialog ref="MessageDialog"></MessageDialog>
        <div
          style={{
            display: this.state.expanded ? "inline-block" : "none",
            margin: "10px",
            background: this.props.racun.ugasen
              ? "#757575"
              : this.props.background,
            boxShadow: this.props.racun.ugasen
              ? "0"
              : "0px 0px 15px 0px " + this.props.shadow,
          }}
          className="account-card"
        >
          {/* <IconButton className='expand-button' onClick={() => this.props.openCard(this.props.position)}> */}
          <IconButton
            style={{
              display: this.props.racun.ugasen ? "none" : "inline-block",
            }}
            className="expand-button"
            onClick={() => this.expand()}
          >
            <AspectRatioIcon fontSize="large" style={{ color: "#fff" }} />
          </IconButton>

          <p className="account-owner">
            {this.props.user.ime} {this.props.user.prezime}
          </p>
          <p className="account-number">
            {this.props.user.racuni[this.props.position].brojRacuna}{" "}
          </p>
          <p className="balance">
            {Math.ceil(
              this.props.user.racuni[this.props.position].trenutnoStanje * 100
            ) / 100}{" "}
            {this.props.user.racuni[this.props.position].valuta.skraceniNaziv}
          </p>
          <i
            style={{
              float: "right",
              color: "#fff",
              fontSize: "50px",
              marginRight: "20px",
              marginBottom: "200px",
            }}
            class="fab fa-cc-visa"
          ></i>
        </div>

        <div
          style={{
            display: this.state.expanded ? "none" : "inline-block",
            margin: "10px",
            background: this.props.background,
            boxShadow: "0px 0px 15px 0px " + this.props.shadow,
          }}
          className="account-card-expanded"
        >
          <IconButton className="expand-button" onClick={() => this.expand()}>
            <AspectRatioIcon fontSize="large" style={{ color: "#fff" }} />
          </IconButton>

          <p className="account-owner-expanded">
            {this.props.user.ime} {this.props.user.prezime}
          </p>
          <p className="account-number-expanded">
            {this.props.user.racuni[this.props.position].brojRacuna}{" "}
          </p>
          <p className="balance-expanded">
            {Math.ceil(
              this.props.user.racuni[this.props.position].trenutnoStanje * 100
            ) / 100}{" "}
            {this.props.user.racuni[this.props.position].valuta.skraceniNaziv}
          </p>
          <Button
            onClick={() => this.disableAcc()}
            variant="contained"
            size="medium"
            style={{
              color: this.state.clr,
              background: "#fff",
              marginLeft: "2%",
            }}
          >
            Disable Account
          </Button>
          <div
            style={{
              display: this.state.disableAccount ? "inline-block" : "none",
              width: "100%",
            }}
          >
            <TableContainer
              style={{ width: "96%", marginLeft: "2%" }}
              component={Paper}
            >
              <Table aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell align="right">Datum</TableCell>
                    <TableCell align="right">Novo Stanje</TableCell>
                    <TableCell align="right">Prethodno Stanje</TableCell>
                    <TableCell align="right">Stanje na teret</TableCell>
                    <TableCell align="right">Stanje u korist</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>{this.renderDnevnaStanja()}</TableBody>
              </Table>
            </TableContainer>
          </div>

          <div
            style={{
              display: this.state.disableAccount ? "none" : "inline-block",
              width: "50%",
              marginLeft: "10%",
            }}
          >
            <p style={{ color: "white" }}>Broj racuna za prenos</p>
            <input
              name="racunZaPrenos"
              style={{ width: "300px" }}
              value={this.state.racunZaPrenos}
              onChange={this.handleChange.bind(this)}
              className="input-field-client"
            ></input>
            <Button
              variant="contained"
              size="medium"
              style={{
                display: "block",
                color: this.state.clr,
                background: "#fff",
                marginTop: "20px",
                marginLeft: "2%",
              }}
              onClick={() => this.handleGasenjeRacuna()}
            >
              Potvrdi gasenje
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

export default AccountsDialog;
