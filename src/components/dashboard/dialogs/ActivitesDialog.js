import React, { Component, Fragment } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import axios from 'axios';
import IconButton from '@material-ui/core/IconButton';
import CreditCardIcon from '@material-ui/icons/CreditCard';
import Divider from '@material-ui/core/Divider';
import AccountCard from './AccountCard'
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import AddBoxRoundedIcon from '@material-ui/icons/AddBoxRounded';
import AccountForm from '../accounts/AccountForm'
import './Accounts.css';


export class ActivitiesDialog extends Component {

    state = {
        open: false,
    }


    handleToggle = () => {
            this.setState({
                open: !this.state.open
            }) ;        
    }


   

    renderActivities = () => {
        let components = [];
        for(let i = 0; i < this.props.user.delatnosti.length ; i++){
            let delatnost = this.props.user.delatnosti[i]
            components.push(
                <div style={this.activitiesStyle}>
                    <p style={this.activitiesText}>{delatnost.naziv}</p>
                    <p style={this.activitiesText}>{delatnost.sifra}</p>
                </div>           
            )
        }
    
        return components;
    }

    render() {
        const{open} = this.state
        return (
            <Fragment>
              
                <IconButton onClick={this.handleToggle}>
                    <CreditCardIcon fontSize="large" style={{color:'#000'}}/>
                  </IconButton>
                <Dialog
                 PaperProps = {{
                    style: {
                        borderRadius: '20px',
                        width: '70vw',
                        height: '70vh',
                      },
                }}
                    fullScreen 
                    open={open}
                    onClose={this.handleToggle}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{this.props.user.prezime + ' ' + this.props.user.ime + " delatnosti "}
               
                    <Button style={{float:'right'}} color="secondary" onClick={this.handleToggle}>
                        Close
                    </Button>
                    </DialogTitle>
                    <Divider></Divider>
                    <DialogContent >
                    
                    {this.renderActivities()}
                                     
                    
                    </DialogContent>
                </Dialog>               
                
            </Fragment>
        )
    }   

    activitiesStyle = {
        paddingTop: '40px',
        height: '150px',
        width: '30%',
        borderRadius: '20px',
        display: 'inline-block',
        margin: '1%',
        boxShadow: '0px 0px 10px -2px rgba(0,0,0,0.75)',
        background: 'linear-gradient(45deg, rgba(240,47,194,1) 9%, rgba(96,148,234,1) 90%)',
    }

    activitiesText = {
        textAlign: 'center',
      color: '#FFFFFF',
      fontSize: '17px',
      margin: '0',
      fontWeight: '500',
      marginTop: '4px'
    }
}

export default ActivitiesDialog