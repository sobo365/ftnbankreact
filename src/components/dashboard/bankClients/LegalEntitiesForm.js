import React, { Component } from "react";
import "../Dashboard.css";
import "./bankClients.css";
import axios from "axios";
import ClientActivity from "./ClientActivity";
import { validate, Validator } from "../../../util/Validator";
import MessageDialog from '../dialogs/MessageDialog';

export class LegalEntitiesForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ime: "",
      prezime: "",
      adresa: "",
      telefon: "",
      jmbg: "",
      errors: {},
      availableActivities: [],
      activities: [],
      ids: [],
      valute: [],
      naziv: '',
      pib: ''
    };
  }

  componentDidMount() {
    axios({
      method: "GET",
      url: "http://localhost:8080/api/delatnosti",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("bank_jwt"),
      },
    })
      .then((response) => {
        console.log(response);
        this.setState({
          availableActivities: response.data,
        });
      })
      .catch(function (error) {
        console.log(error);
        alert("error - individual form - component did mount");
      })
      .then(function () {
        // always executed
      });
  }

  handleChange = (e) => {
    if (e.target.name === "ime") {
      if (validate(e.target.value, [Validator.REQUIRED])) {
        this.state.errors.ime = true;
      } else {
        this.state.errors.ime = false;
      }
    }
    if (e.target.name === "prezime") {
      if (validate(e.target.value, [Validator.REQUIRED])) {
        this.state.errors.prezime = true;
      } else {
        this.state.errors.prezime = false;
      }
    }
    if (e.target.name === "adresa") {
      if (validate(e.target.value, [Validator.REQUIRED, Validator.ADDRESS])) {
        this.state.errors.adresa = true;
      } else {
        this.state.errors.adresa = false;
      }
    }
    if (e.target.name === "telefon") {
      if (validate(e.target.value, [Validator.REQUIRED, Validator.PHONE])) {
        this.state.errors.telefon = true;
      } else {
        this.state.errors.telefon = false;
      }
    }
    if (e.target.name === "jmbg") {
      if (validate(e.target.value, [Validator.REQUIRED, Validator.JMBG])) {
        this.state.errors.jmbg = true;
      } else {
        this.state.errors.jmbg = false;
      }
    }

    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  renderActivities() {
    let components = [];
    for (let i = 0; i < this.state.activities.length; i++) {
      let activity = this.state.activities[i];
      components.push(
        <ClientActivity
          key={i}
          position={i}
          removeActivity={this.removeActivity.bind()}
          activities={this.state.availableActivities}
          discardNew={this.discardNewActivity.bind()}
          saveActivity={this.saveActivity.bind()}
          activity={activity}
        ></ClientActivity>
      );
    }

    return components;
  }

  render() {
    return (
      <div className="dashboard-container-empty">
         <MessageDialog ref = 'MessageDialog'></MessageDialog> 
        <div className="header">
          <p className="client-header-title">Add Legal Entitiy</p>
          <button onClick={this.submit} className="submit-btn">
            Submit
          </button>
          <button onClick={this.discard} className="discard-btn">
            Discard
          </button>
        </div>
        <div className="form-wrapper">
          <p>Ime</p>
          <input
            name="ime"
            value={this.state.ime}
            onChange={this.handleChange.bind(this)}
            className={
              this.state.errors.ime
                ? "input-field-client-error"
                : "input-field-client"
            }
          ></input>
          <p>Prezime</p>
          <input
            name="prezime"
            value={this.state.prezime}
            onChange={this.handleChange.bind(this)}
            className={
              this.state.errors.prezime
                ? "input-field-client-error"
                : "input-field-client"
            }
          ></input>
          <p>Adresa</p>
          <input
            name="adresa"
            value={this.state.adresa}
            onChange={this.handleChange.bind(this)}
            className={
              this.state.errors.adresa
                ? "input-field-client-error"
                : "input-field-client"
            }
          ></input>
          <p>Telefon</p>
          <input
            name="telefon"
            value={this.state.telefon}
            onChange={this.handleChange.bind(this)}
            className={
              this.state.errors.telefon
                ? "input-field-client-error"
                : "input-field-client"
            }
          ></input>
          <p>JMBG</p>
          <input
            name="jmbg"
            value={this.state.jmbg}
            onChange={this.handleChange.bind(this)}
            className={
              this.state.errors.jmbg
                ? "input-field-client-error"
                : "input-field-client"
            }
          ></input>
          <p>Naziv</p>
          <input
            name="naziv"
            value={this.state.naziv}
            onChange={this.handleChange.bind(this)}
            className='input-field-client'
          ></input>
          <p>PIB</p>
          <input
           name="pib"
           value={this.state.pib}
           onChange={this.handleChange.bind(this)}
           className='input-field-client'
          ></input>
        </div>
        <div className="activity-wrapper">
          <p style={{ textAlign: "center" }}>Delatnosti</p>
          <hr></hr>

          <div className="activity-list-wrapper">
            {this.renderActivities()}
            <div
              onClick={this.addEmptyActitvity}
              className="add-empty-activity"
            >
              <p>Add</p>
            </div>
          </div>
        </div>
      </div>
    );
  }

  saveActivity = (id, naziv, sifra) => {
    this.state.activities.pop([this.state.activities.length - 1]);
    this.state.activities.push({
      id: id,
      sifra: sifra,
      naziv: naziv,
      new: false,
    });
    this.state.ids.push(id);
    this.forceUpdate();
  };

  discardNewActivity = () => {
    this.state.activities.pop([this.state.activities.length - 1]);
    this.forceUpdate();
  };

  removeActivity = (position) => {
    alert(position);
    this.state.activities.pop(position + 1);
    this.forceUpdate();
  };

  addEmptyActitvity = () => {
    console.log(this.state.activities);
    if (this.state.activities.length == 0) {
      this.state.activities.push({ new: true });
    } else if (
      this.state.activities[this.state.activities.length - 1].new == false
    ) {
      this.state.activities.push({ new: true });
    }
    this.forceUpdate();
  };

  discard = () => {
    this.props.history.go(-1);
  };

  submit = () => {
    console.log(this.state.errors);
    const err = Object.values(this.state.errors);
    console.log(err);
    if (err.length != 5) {
    } else if (err.includes(true)) {
      alert("ima greska");
    } else if (this.state.activities.length < 1) {
      this.refs.MessageDialog.handleToggle('Pravni klijen mora da ima bar jednu aktivnu delatnost!', true);
    } else {
      console.log({
        delatnosti: this.state.activities,
      });
      axios({
        method: "POST",
        url: "http://localhost:8080/api/pravni-klijenti",
        headers: {
          Authorization: "Bearer " + localStorage.getItem("bank_jwt"),
        },
        data: {
          ime: this.state.ime,
          prezime: this.state.prezime,
          adresa: this.state.adresa,
          jmbg: this.state.jmbg,
          telefon: this.state.telefon,
          racuni: [],
          delatnosti: this.state.activities,
          naziv: this.state.naziv,
          pib: this.state.pib
        },
      })
        .then((response) => {
          console.log("----- OVO JE RESPONSE----");
          console.log(response);
          localStorage.setItem("new_account_id", response.data.id);
          localStorage.setItem("createdUserFirstName", this.state.ime);
          localStorage.setItem("createdUserLastName", this.state.prezime);
          localStorage.setItem("createdUserAddress", this.state.adresa);
          localStorage.setItem("createdUserJmbg", this.state.jmbg);
          localStorage.setItem("createdUserPhone", this.state.telefon);
          this.refs.MessageDialog.handleToggle('Klijent uspesno kreiran!', false);
          this.props.history.push("/dashboard/accounts/add-account");
        })
        .catch((error) => {
          console.log(error.response);
          try{
            this.refs.MessageDialog.handleToggle(error.response.data.greske.greska, true);
          } catch {
            this.refs.MessageDialog.handleToggle('Greska na serveru', true);
          }
          
        })
        .then(function () {
          // always executed
        });
    }
  };
}

export default LegalEntitiesForm;
