import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import axios from 'axios';
import '../Dashboard.css';
import './bankClients.css';
import UsersTable from '../table/UsersTable';
import MessageDialog from '../dialogs/MessageDialog';

export class Individuals extends Component {

  tableHeader = ["Ime", "Prezime", "Adresa", "Telefon", "JMBG", "Racuni"]

  constructor(props){
    super(props);

    this.state = {
        individuals: [],
    }
  }

  componentWillMount(){
    axios({
      method: 'get',
      url: 'http://localhost:8080/api/fizicki-klijenti',
     headers: {
       Authorization: 'Bearer ' + localStorage.getItem('bank_jwt')
     }
    }).then((response) => {
        console.log(response)
        this.setState({
          individuals: response.data
        })
      })
      .catch(function (error) {
        console.log(error)
        this.refs.MessageDialog.handleToggle(error.response.data.greske.greska, true);
        
      })
      .then(function () {
        // always executed
      });

     
  }


  render() {
    return (
      <div className="dashboard-container-empty">
         <MessageDialog ref = 'MessageDialog'></MessageDialog> 
         <div className = "header">
         <p className = 'client-header-title'>Individuals</p>
            <NavLink to="/dashboard/clients/add-individual"  className='add-client-btn'>Add Individual</NavLink>
         </div>
        <UsersTable  data={this.state.individuals} header={this.tableHeader}></UsersTable>        
        </div>
    )
}

  
}





export default Individuals;