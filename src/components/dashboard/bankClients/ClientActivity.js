import React, { Component } from 'react';
import '../Dashboard.css';
import './bankClients.css';
import axios from 'axios';

export class ClientActivity extends Component {

  constructor(props){
    super(props);
      this.state ={
        sifra: '',
      }
  }


  renderActivities = () =>{
    let components = []

    components.push(
      <option
           name=""
           key={-1}
           value="">
           </option>
      )

    for(let i = 0; i  < this.props.activities.length; i++){
      let activity = this.props.activities[i];
      let val = activity.id + '-' + activity.sifra + '-' + activity.naziv
      components.push(
      <option
           name={activity.sifra}
           key={i}
           value={val}>{activity.naziv}
           </option>
      )
    }

    return components
  }

  handleChange = (e) =>{
    let vals = e.target.value.split('-')
    this.state.id=vals[0]
    this.state.sifra=vals[1]
    this.state.naziv=vals[2]
    this.forceUpdate()
  }

  render() {
    return (
      <div className="client-activity">
        <p hidden={this.props.activity.new ? true : false} style={{textAlign:'center', margin:0}}>{this.state.sifra}</p>
        <p hidden={this.props.activity.new ? true : false} style={{textAlign:'center'}}>{this.state.naziv}</p>
        <p style={{textAlign:'center'}}>
          <select
          hidden={this.props.activity.new ? false : true}
          onChange={this.handleChange}
          className='selectBox'
          disabled={this.props.activity.new ? false : true} 
          >
          {this.renderActivities()}
          </select>
        </p>
      

       
        <button 
                onClick={() => this.submit()} 
                hidden={this.props.activity.new ? false : true}
                className='submit-btn'
                >Add</button>
        <button 
                onClick={() => this.props.discardNew()}
                hidden={this.props.activity.new ? false : true}
                className='discard-btn'
                >Discard</button>
        <button 
                onClick={() => this.props.removeActivity(this.props.position)}
                hidden={this.props.activity.new ? true : false}
                className='discard-btn'
                >Remove</button>
      </div>
    )
}


  submit = () =>{
      if(this.state.naziv === undefined){
        alert('Select value or discard');
      }else{
        this.props.saveActivity(this.state.id, this.state.naziv, this.state.sifra)
      }
  }
          
    
  
}





export default ClientActivity;