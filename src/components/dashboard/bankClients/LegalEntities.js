import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import '../Dashboard.css';
import './bankClients.css';
import axios from 'axios';
import LegalsTable from '../table/LegalsTable'

export class LegalEntities extends Component {

  tableHeader = ["Ime", "Prezime", "Adresa", "Telefon", "JMBG", "Racuni", "Naziv", "PIB"]

  constructor(props){
    super(props);

    this.state = {
        clients: [],
    }
  }

  componentWillMount(){
    axios({
      method: 'get',
      url: 'http://localhost:8080/api/pravni-klijenti',
     headers: {
       Authorization: 'Bearer ' + localStorage.getItem('bank_jwt')
     }
    }).then((response) => {
        console.log(response)
        this.setState({
          clients: response.data
        })
      })
      .catch(function (error) {
        console.log(error)
        alert('error')
        
      })
      .then(function () {
        // always executed
      });

     
  }

  render() {
    return (
      <div className="dashboard-container-empty">
         <div className = "header">
         <p className = 'client-header-title'>Legal entities</p>
         <NavLink to="/dashboard/accounts/add-legal"  className='add-client-btn'>Add Legal Entity</NavLink>
         </div>
         <LegalsTable  data={this.state.clients} header={this.tableHeader}></LegalsTable>
      </div>
    )
}

  
}
export default LegalEntities;