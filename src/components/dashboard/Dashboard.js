import React, {Component} from 'react';
import SideNav from './sidenav/SideNav';
import './Dashboard.css';
import { Link, Route, Switch } from 'react-router-dom'; 
import PaymentOrder from './payments/PaymentOrder';
import PaymentCheck from './payments/PaymentCheck';
import TransferOrder from './payments/TransferOrder';
import CollectionOrder from './payments/CollectionOrder';
import Individuals from './bankClients/Individuals';
import LegalEntities from './bankClients/LegalEntities';
import IndividualForm from './bankClients/IndividualForm';
import AccountForm from './accounts/AccountForm';
import LegalEntitiesForm from './bankClients/LegalEntitiesForm';




export class Dashboard extends Component {

  componentWillMount(){
    if(localStorage.getItem('bank_jwt') == null) {
      window.location.replace('/')
    }
  }
  
  render() {
    return (
      <div className="dashboard-wrapper">
          <SideNav></SideNav>
          <Switch>
            <Route path='/dashboard/payments/payment-order' component={PaymentOrder} ></Route>
            <Route path='/dashboard/payments/payment-check' component={PaymentCheck} ></Route>
            <Route path='/dashboard/payments/transfer-order' component={TransferOrder} ></Route>
            <Route path='/dashboard/payments/collection-order' component={CollectionOrder} ></Route>
            <Route path='/dashboard/clients/individuals' component={Individuals} ></Route>
            <Route path='/dashboard/clients/legal-entities' component={LegalEntities} ></Route>
            <Route path='/dashboard/clients/add-individual' component={IndividualForm} ></Route>
            <Route path='/dashboard/accounts/add-account' component={AccountForm} ></Route>
            <Route path='/dashboard/accounts/add-legal' component={LegalEntitiesForm} ></Route>
  
          </Switch> 
      </div>
    );
  }
  
}



export default Dashboard;
