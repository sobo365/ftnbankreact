import React, { Component } from "react";
import "../Dashboard.css";
import "./Accounts.css";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import axios from "axios";
import MessageDialog from "../dialogs/MessageDialog";

export class AccountForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currency: [],
      firstName: "",
      lastName: "",
      address: "",
      phone: "",
      jmbg: "",
      currencyId: 2,
    };
  }

  handleChange = (e) => {
    this.setState({
      currencyId: e.target.value,
    });
  };

  componentDidMount() {
    //id kad bude dostupan
    //alert(this.props.user.ime)
    axios({
      method: "get",
      url: "http://localhost:8080/api/valute",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("bank_jwt"),
      },
    })
      .then((response) => {
        console.log(response);
        this.setState({
          currency: response.data,
        });
      })
      .catch((error) => {
        console.log(error);
        this.refs.MessageDialog.handleToggle(
          "Greška prilikom učitavanja valuta!",
          true
        );
      })
      .then(function () {
        // always executed
      });

    if (this.props.user != null) {
      this.setState({
        ime: this.props.user.ime,
        prezime: this.props.user.prezime,
        adresa: this.props.user.adresa,
        telefon: this.props.user.telefon,
        jmbg: this.props.user.jmbg,
      });
    } else {
      this.setState({
        ime: localStorage.getItem("createdUserFirstName"),
        prezime: localStorage.getItem("createdUserLastName"),
        adresa: localStorage.getItem("createdUserAddress"),
        telefon: localStorage.getItem("createdUserPhone"),
        jmbg: localStorage.getItem("createdUserJmbg"),
      });
    }
  }

  requestAcc = () => {
    if (localStorage.getItem("new_account_id") !== null) {
      axios({
        method: "POST",
        url: "http://localhost:8080/api/racuni",
        data: {
          bankaId: 1,
          klijentId: localStorage.getItem("new_account_id"),
          valutaId: this.state.currencyId,
        },
        headers: {
          Authorization: "Bearer " + localStorage.getItem("bank_jwt"),
        },
      })
        .then((response) => {
          this.refs.MessageDialog.handleToggle(
            "Racun je uspseno kreiran!",
            false
          );
          console.log(response);
          localStorage.removeItem("new_account_id");
          localStorage.removeItem("createdUserFirstName");
          localStorage.removeItem("createdUserLastName");
          localStorage.removeItem("createdUserAddress");
          localStorage.removeItem("createdUserPhone");
          localStorage.removeItem("createdUserJmbg");
        })
        .catch((error) => {
          console.log(error);
          try {
            this.refs.MessageDialog.handleToggle(
              error.response.data.greske.greska,
              true
            );
          } catch {
            this.refs.MessageDialog.handleToggle("Greška na serveru!", true);
          }
        })
        .then(function () {
          // always executed
        });
    } else {
      axios({
        method: "POST",
        url: "http://localhost:8080/api/racuni",
        data: {
          bankaId: 1,
          klijentId: this.props.user.id,
          valutaId: this.state.currencyId,
        },
        headers: {
          Authorization: "Bearer " + localStorage.getItem("bank_jwt"),
        },
      })
        .then((response) => {
          this.refs.MessageDialog.handleToggle(
            "Racun je uspseno kreiran!",
            false
          );
        })
        .catch((error) => {
          console.log(error);
          try {
            this.refs.MessageDialog.handleToggle(
              error.response.data.greske.greska,
              true
            );
          } catch {
            this.refs.MessageDialog.handleToggle("Greška na serveru!", true);
          }
        })
        .then(function () {
          // always executed
        });
    }
  };

  renderCurrency = () => {
    let components = [];
    for (let i = 0; i < this.state.currency.length; i++) {
      let curr = this.state.currency[i];
      components.push(
        <MenuItem key={i} value={i + 1}>
          {curr.skraceniNaziv} - {curr.nazivValute}
        </MenuItem>
        //value={curr.id}>{curr.skraceniNaziv} - {curr.nazivValute}
      );
    }
    return components;
  };

  render() {
    return (
      <div
        className={this.props.user ? "dialog-container" : "dashboard-container"}
      >
        <MessageDialog ref="MessageDialog"></MessageDialog>
        <div>
          <p className="client-header-title">Account Owner</p>
          <hr></hr>
          <p className="account-data">Ime: {this.state.ime}</p>
          <p className="account-data">Prezime: {this.state.prezime}</p>
          <p className="account-data">Adresa: {this.state.adresa}</p>
          <p className="account-data">Telefon: {this.state.telefon}</p>
          <p className="account-data">Jmbg: {this.state.jmbg}</p>

          <FormControl variant="filled" style={{ margin: "20px" }}>
            <InputLabel
              style={{ width: "300px" }}
              id="demo-simple-select-label"
            >
              Currency
            </InputLabel>
            <Select
              onChange={this.handleChange}
              value={this.currencyId}
              defaultValue={2}
              style={{ width: "300px" }}
              labelId="demo-simple-select-label"
            >
              {this.renderCurrency()}
            </Select>
          </FormControl>
        </div>

        <div className="actions-footer">
          <button onClick={this.requestAcc} className="submit-btn">
            Request Account
          </button>
          <button onClick={this.discard} className="discard-btn">
            Discard
          </button>
        </div>
      </div>
    );
  }
}

export default AccountForm;
