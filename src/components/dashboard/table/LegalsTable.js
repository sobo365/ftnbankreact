import React, { Component } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TablePagination from '@material-ui/core/TablePagination';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import AccountsDialog from "../dialogs/AccountsDialog";
import ActivitiesDialog from '../dialogs/ActivitesDialog';


export class LegalsTable extends Component {
 
     constructor(props){
        super(props);
    
        this.state = {
            header: [],
            data: '',   
        }
      }  

      renderHeader = () => {
        let components = [];
        for(let i = 0; i < this.props.header.length; i++){
          let item = this.props.header[i];
          components.push(
           
                

          )
        }
    
        return components;
      }

      renderData = () => {
        let components = [];
        console.log(".....")
        console.log(this.props.data)
        for(let i = 0; i < this.props.data.length; i++){
          let item = this.props.data[i];
          console.log(this.props.data[i])
          components.push(
            <TableRow key={i}>
                <TableCell align="right">{item.ime}</TableCell>
                <TableCell align="right">{item.prezime}</TableCell>
                <TableCell align="right">{item.adresa}</TableCell>
                <TableCell align="right">{item.telefon}</TableCell>
                <TableCell align="right">{item.jmbg}</TableCell>
                <TableCell align="right">{item.naziv}</TableCell>
                <TableCell align="right">{item.pib}</TableCell>
                <TableCell align="right"><ActivitiesDialog user={item}></ActivitiesDialog></TableCell>
                <TableCell align="right"><AccountsDialog user={item}></AccountsDialog></TableCell>
            </TableRow>
                
          )
        }
    
        return components;
      }

    render() {
        return (
            <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableHead>
    
                
                <TableRow >
              <TableCell align="right">Ime</TableCell>
              <TableCell align="right">Prezime</TableCell>
              <TableCell align="right">Adresa</TableCell>
              <TableCell align="right">Telefon</TableCell>
              <TableCell align="right">JMBG</TableCell>
              <TableCell align="right">Naziv</TableCell>
              <TableCell align="right">PIB</TableCell>
              <TableCell align="right">Delatnosti</TableCell>
              <TableCell align="right">Racuni</TableCell>
      
                </TableRow>
              </TableHead>
              <TableBody>
               {this.renderData()}
              </TableBody>
            </Table>
          </TableContainer>
        )
    }

}
export default LegalsTable;