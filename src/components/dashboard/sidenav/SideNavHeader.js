import React, { Component } from 'react';


export class SideNav extends Component {

    render() {
        return (
            <div style={this.style}>
                <h2>FTNBank</h2>
            </div>
        );
    }

    style = {
        color: 'rgba(255,69,58,1)',
        margin: '20px',
        marginBottom: '40px',
        borderBottom: '1px solid rgba(255,69,58,0.4)'
    }

}

export default SideNav