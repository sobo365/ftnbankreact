import React, { Component } from 'react';
import SideNavHeader from './SideNavHeader';
import { NavLink } from 'react-router-dom';
import './SideNav.css';
import './SideNavLink.css';


export class SideNav extends Component {

    logout() {
        localStorage.removeItem('bank_jwt')
        window.location.replace('/')
    }

    render() {
        return (
            <div className="sidenav-wrapper">
                <SideNavHeader></SideNavHeader>
               <ul>
                   
                       <h4>Home</h4>
                       <ul>
                           <li> <NavLink exact to = '/dashboard/'  className="sidebarLink" activeClassName="sidebarLinkActive">Home</NavLink></li>
                       </ul>
                       <h4>Payments</h4>
                      
                        <ul style={{width: '100%'}}>
                            <li> <NavLink to = '/dashboard/payments/payment-order'  className="sidebarLink" activeClassName="sidebarLinkActive">Payment Order</NavLink></li>
                            <li> <NavLink to = '/dashboard/payments/payment-check'  className="sidebarLink" activeClassName="sidebarLinkActive">Payment Check</NavLink></li>
                            <li> <NavLink to = '/dashboard/payments/transfer-order'  className="sidebarLink" activeClassName="sidebarLinkActive">Transfer Order</NavLink></li>
                            <li> <NavLink to = '/dashboard/payments/collection-order'  className="sidebarLink" activeClassName="sidebarLinkActive">Collection Order</NavLink></li>
                        </ul>
                       <h4>Clients</h4>
                       <ul>
                       
                            <li> <NavLink to = '/dashboard/clients/individuals'  className="sidebarLink" activeClassName="sidebarLinkActive">Individuals</NavLink></li>
                            <li> <NavLink to = '/dashboard/clients/legal-entities'  className="sidebarLink" activeClassName="sidebarLinkActive">Legal Entities</NavLink></li>
                    </ul>
               </ul>
               <div className='footer'>
                   <p onClick={this.logout}>Logout</p>
               </div>
            </div>
        )
    }

  
    

}

export default SideNav
