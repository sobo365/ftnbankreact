export const Validator = {
    REQUIRED: 'required',
    ADDRESS: 'address',
    PHONE: 'phone',
    JMBG: 'jmbg'
}

let addressRegex = new RegExp('\\w([A-z]{1,} {0,1})+[0-9]{1,5}[A-z]{0,1}');
let phoneRegex = new RegExp('\\+[0-9]{11,12}$');
let jmbgRegex = new RegExp('^[0-9]{13}$');

export const validate = (value, [...args]) =>{

    if(args.includes(Validator.REQUIRED)){
        if(value.trim() === '' || value === null){
            return true;
        }
    }
    if(args.includes(Validator.ADDRESS)){
        if(!addressRegex.test(value)){
            return true;
        }
    }
    if(args.includes(Validator.PHONE)){
        if(!phoneRegex.test(value)){
            return true;
        }
    }
    if(args.includes(Validator.JMBG)){
        if(!jmbgRegex.test(value)){
            return true;
        }
    }

    return false;

}

