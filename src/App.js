import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import LandingPage from './components/landingPage/LandingPage';
import Dashboard from './components/dashboard/Dashboard';
import SideNav from './components/dashboard/sidenav/SideNav';
import LoginPage from './components/loginPage/LoginPage';



function App() {
  return (
    <Router>
      <div>
        <Switch>
          <Route path="/" exact component={LandingPage} />
          <Route path="/dashboard" component={Dashboard} />
          <Route path="/login" component={LoginPage} />
        </Switch>
        
      </div>
    </Router>
    
  );
}





export default App;
